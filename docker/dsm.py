#! /usr/bin/python3
import asyncio
import websockets
import json
import logging
import aiohttp

from typing import Any

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

SERVICE_ID = "dynamic-spectrum-manager"

async def main():
    ipv6 = await get_ipv6()
    service_url = f"ws://[{ipv6}]:8765/"

    logging.info(f"Announcing DSM service as {service_url} under name {SERVICE_ID}.")
    await announce_service(service_url, SERVICE_ID)

    logging.info(f"Starting DSM service on {service_url} ...")
    await serve_dsm(service_url)

async def get_ipv6():
    async with aiohttp.ClientSession().get("http://localhost:8080/node-id") as response:
        nid = (await response.json(content_type=None))["node-id"]
        logging.info("Retrieved own node-id: %s", nid)

        # convert to proper ip
        nid = bytes.fromhex(nid)
        nip = f"fc00:{nid.hex(':', 2)}"
        return nip

async def announce_service(service_url: str, service_id: str):
    """Announce service under id.

    Stores service url in the network DHT under specified service_id to make service discoverable
    """
    # store service_url under well-known name 
    async with aiohttp.ClientSession().post("http://localhost:8080/dht/store", 
            params={"key": service_id}, data=service_url) as response:

        if response.status in range(200, 300):
            logging.info("Announced service under id %s to the network: %s", service_id, service_url)
        else:
            logging.error("Announcing service over dht not possible: %s", store_r)

async def serve_dsm(ipv6):
    async with websockets.serve(handle_client, None, 8765):
        await asyncio.Future()  # run forever

async def handle_client(websocket):
    registration = await websocket.recv()
    logging.info(f"<<< {registration}")

    echo = f"Echo: {registration}!"

    await websocket.send(echo)
    logging.info(f">>> {echo}")


if __name__ == "__main__":
    # start http server
    asyncio.run(main())
