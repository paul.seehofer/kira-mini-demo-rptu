#! /usr/bin/python3
import asyncio
import websockets
import json
import logging
import aiohttp
import base64
import socket

from typing import Any

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

SERVICE_ID = "dynamic-spectrum-manager"

async def main():
    logging.info(f"Looking up DSM service under name {SERVICE_ID}.")
    service_url = await discover_service(SERVICE_ID)

    logging.info(f"Connecting to DSM using {service_url} ...")
    await connect(service_url)


async def discover_service(service_id: str):
    """Discover service under id.

    Fetches service url stored in the network DHT under specified service_id
    """
    # fetch service_url under well-known name 

    async with aiohttp.ClientSession().get("http://localhost:8080/dht/fetch", 
            params={"key": service_id}) as fetch_r:

        if fetch_r.status in range(200, 300):
            # get json 
            entries = await fetch_r.json(content_type=None)
            
            # take the first entry and decode it to a string
            service_url = str(base64.b64decode(entries[0]), encoding="utf-8")

            logging.info(f"Got service {service_url} under id: {service_id}")
            return service_url
        else:
            logging.error("Could not get service over dht: %s", service_id)

async def connect(service_url):
    async with websockets.connect(service_url) as websocket:
        registration = f"Host {socket.gethostname()} registers!"

        await websocket.send(registration)
        logging.info(f">>> {registration}")

        response = await websocket.recv()
        logging.info(f"<<< {response}")

if __name__ == "__main__":
    asyncio.run(main())
