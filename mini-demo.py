#! /bin/env python3

from mininet.net import Containernet
from mininet.node import Controller
from mininet.cli import CLI
from mininet.log import info, setLogLevel

import argparse

from subprocess import Popen, PIPE
from time import sleep

from re import search

setLogLevel('info')

KIRA_IMAGE="kira-mini-demo:latest"

DCMD = "/usr/bin/supervisord -c /etc/supervisord.conf"
SYSCTLS = {
    'net.ipv6.conf.default.disable_ipv6': 0,
    'net.ipv6.conf.all.forwarding': 1,
    }

def main(args):
    net = Containernet()
    try:
        info('*** Setting up network\n')
        setup(net)
        info('*** Starting network\n')
        net.start()
        info('*** Running CLI\n')
        CLI(net)
    finally:
        info('*** Stopping network')
        net.stop()


def setup(net):
    def addDocker(net, name, image, env={}):
        env.update({'RUST_LOG': "debug"})
        return net.addDocker(name, dimage=image, sysctls=SYSCTLS, ip=None, network_mode="none", dcmd=DCMD, 
                             environment=env)  # share IPC

    info('*** Adding docker containers\n')
    # sim
    dsm = addDocker(net, 'dsm', KIRA_IMAGE)
    erc1 = addDocker(net, 'erc1', KIRA_IMAGE)
    erc2 = addDocker(net, 'erc2', KIRA_IMAGE)

    # kira
    d4, d5, d6, d7 = [ addDocker(net, f"d{i}", KIRA_IMAGE) for i in  range(4,8) ]

    # add links
    net.addLink(dsm, d6)
    net.addLink(erc1, d4)
    net.addLink(erc2, d5)
    net.addLink(erc2, d7)
    net.addLink(d4, d5)
    net.addLink(d5, d6)
    net.addLink(d7, d6)

def exec_node(node, cmd):
    info(f"Executing CMD on {node.name}: {cmd}")
    return node.popen(cmd, shell=True, stdout=PIPE).stdout.read(-1).decode()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Static 6G Demo')
    args = parser.parse_args()
    main(args)

