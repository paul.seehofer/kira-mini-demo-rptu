DOCKER_DIR = docker
IMAGE_URL = https://bwsyncandshare.kit.edu/s/BPozqyL3rda5orH/download

.PHONY: load-kira-image
load-kira-image:
	curl ${IMAGE_URL} | zcat | docker load

.PHONY: build-image
build-image:
	docker build -t kira-mini-demo:latest -f ${DOCKER_DIR}/Dockerfile docker
