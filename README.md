# KIRA-Demo

This demo uses KIRA on a network to establish connectivity between all nodes.

It also includes two scripts that emulate the discovery and connectivity between a _dynamic spectrum manager (DSM)_ service and a corresponding _echo ring controller (ERC)_.

### DSM
The [script](docker/dsm.py) does the following things:
1. Lookup the own node-id and translates it to the local IPv6 address
2. Registers the dsm service in the DHT under a websocket-url
3. Starts a websocket server listening for new connections


### ERC
The [script](docker/client.py) does the following things:
1. Lookup the url of the DSM service in the DHT
2. Open a websocket connection to the DSM service

## Dependencies
- installed [Containernet](https://containernet.github.io/#installation)

## Setup
1. Download and load `sudo make load-kira-image`
2. Build image with scripts included `sudo make build-image`

## Usage
You need three terminals:
1. One in which you start the emulated network: `./mini-demo.py`
2. One in which you connect to the dsm node: `docker exec -it mn.dsm bash`
3. One in which you connect to the erc1 node: `docker exec -it mn.erc1 bash`

Start the DSM service on the dsm node using:
```
./dsm.py
```

Start the ERC client script on the erc1 node using:
```
./client.py
```




